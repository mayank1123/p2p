'''
hostname : Client's name
address : Client's IP address
listenport : port on which the server is listening
listen address : IP address of the server's listener thread
MTU: packet size
server : tuple containing server address and port number
timeout : Time after which client backs off
'''

hostname = "host1" 
address = "192.168.0.18" # client's own address
listenPort = 21
listenAddr = '192.168.0.18'
MTU  = 128
server = ('192.168.0.18' , 5000)
timeout = 0.1