'''
Author: Mayank
'''

# Modules to be used in p2pclient.py

'''
exit_msg : Send the exit message to close the client's connection with server
'''
def exit_msg():
	method = "EXIT " + hostname + " " + address
	c = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	c.bind((address,0))
	c.sendto(method,server)
	c.close()
	exit(0)

'''
sender : generate packets and transmit data
'''
def sender(peer_client,filename):
	header = "200" + " " + "OK" + "\r\n"
	msg = header + filename + " " + str(os.path.getsize(filename))
	peer_client.send(msg)
	with open (filename , 'rb') as f:
		data = f.read(2048)
		peer_client.send(data)
		while data != "":
			data = f.read(2048)
			peer_client.send(data)
		peer_client.close()


'''
p2p_server : Send the requested file to the peer 
'''

def p2p_server(peer_client):
	msg = peer_client.recv(2048)
	msg1 = msg.replace("\r\n" , " " )
	packet_data = msg1.split(" ")			
	filename = packet_data[1]	
	if packet_data[0] == 'GET':
		if os.path.isfile(filename):
			sender(peer_client,filename)
		else:
			header = "404" + " " + "File not found" + "\r\n"
			peer_client.send(header)
	elif os.path.isfile(filename):
		header = "405" + " " + "Method not allowed" + "\r\n"
		peer_client.send(header)
	else:
		header = "400" + " " + "Bad Request" + "\r\n"
		peer_client.send(header)

'''
server_thread: starts the server thread at each peer that listens for incoming connections at listernPort and listernAddr
'''

def server_thread():
	#	print "Listening ... " 
	listener = socket.socket()
	listener.setsockopt(socket.SOL_SOCKET , socket.SO_REUSEADDR,1)
	listener.bind((listenAddr,listenPort))
	listener.listen(5)
	while True:
		peer_client , address = listener.accept()
		process_t = threading.Thread(target = p2p_server,args=(peer_client,))
		process_t.start()
	listener.close()

'''
get_IU_msg : create the packet to be start transmission for inform and update action
'''

def get_IU_msg():
	filename = raw_input("Enter the file name to be updated or type 'q' if you are done updating:")
	msg = ""
	while filename != 'q' :
		if os.path.isfile(filename):
			size = os.path.getsize(filename)
			msg = msg + hostname +" " + filename + " " + str(size) + " " + address + "\r\n"
			filename = raw_input("Enter the file name to be updated or type 'q' if you are done updating:")
		else:
			print "You do not have the '{0}' file, please enter another filename".format(filename)
			filename = raw_input("Enter the file name to be uploaded or type 'q' if you are done updating:")
	return msg	

'''
inform_update : Establish the connection using ACKS(Acknowledgements) and update the requested file
'''
def inform_update():

	DATA = get_IU_msg()

		# --------------------SYN---------------------
	
	seq_no = random.randint(0,7)
	c = socket.socket(socket.AF_INET , socket.SOCK_DGRAM)
	c.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
	c.bind((address,0)) 
	msg= "SYN " + str(seq_no) + " " + '0'
	c.sendto(msg,server)
	print "SENDING SYN ! "

		# ----------------SYNACKACK-------------------
	
	data, thread_addr = c.recvfrom(MTU)
	data = re.split(" ",data)
	if data[0] == "SYNACK": 
		ack_no = (int(data[1])+1)%8 
		seq_no = int(data[2])%8	
		msg = "SYNACKACK " + str(seq_no) + " " + str(ack_no) 
		c.sendto(msg,thread_addr)
		print "SENDING SYNACKACK !! "
		# ------------------DATA----------------------
		
	byte_len = len(DATA)
	count = 0
	end_flag = 1
	timeout = 0.1
	alpha = 0.125

	while count<(byte_len): 	
		success = 0
		while(success!=1):
						# CREATE AND SEND MSG
			header = "I&A "+hostname+" "+address+"\r\n"+str(end_flag)+" "+str(seq_no)+" "+str(ack_no)+"\r\n\r\n" 
			send_size = min((128-len(header)),len(DATA) - count)
			http_msg = header + DATA[count:count+send_size]
			c.sendto(http_msg,thread_addr)
						
							#  TIMER_START
			start_timer = time.time()
			iter_timer = timeout 
			print "Entering inner loop"
						
						# KEEP LISTENING UNTIL TIMEOUT
			while (time.time() - start_timer)< timeout and iter_timer >0:
				ready = select.select([c],[],[],iter_timer)
				if ready[0]:
					sampleRTT = time.time() - start_timer
					timeout = (1-alpha)*timeout + alpha*sampleRTT
					data,addr = c.recvfrom(MTU) 
					data = data.replace("\r\n" , " ")
					data = re.split(" ", data)
					print data	

						# DUPLICATE/MANGLED ACK 
					if data[0] != "ACK" or (int(data[2])) !=((seq_no+1))%8: 
						print "MANGELED / OUT OF ORDRE PACKET"
						timelost = time.time()- start_timer
						if timelost > timeout:
							print "Retransmission due to DUPACK:"
							break
						else: 
							iter_timer = timeout - timelost
					else:
					# NOT DUPLICATE - GENUINE ACK
						print "Packet transmitted successfully ! The force is strong with you !!"
						success = 1
						count+=send_size
						seq_no = int(data[2])%8
						ack_no = (int(data[1])+1)%8
						break
				else:
					print "Retransmission due to Time out:"
		
		# send a msg with End_flag =0 
	
	header = "I&A "+hostname+" "+address+"\r\n"+"0"+" "+str(seq_no)+" "+str(ack_no)+"\r\n\r\n"
	c.sendto(header,thread_addr)
	print "Database Updated ! Closing SOCKET ! Danke Scheon !"
	c.close()