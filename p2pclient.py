import socket
import cPickle as pickle 
import os
import math
import threading 
import random
import re
import time
import select
from client_mods import *
from client_config import *


def content_query():
	
	print "Quering server "

	# --------------------SYN---------------------
	
	seq_no = random.randint(0,7)
	c = socket.socket(socket.AF_INET , socket.SOCK_DGRAM)
	c.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
	c.bind((address,0)) 
	msg= "SYN " + str(seq_no) + " " + '0'
	c.sendto(msg,server)

	# ----------------SYNACKACK-------------------
	
	data, thread_addr = c.recvfrom(MTU)
	data = re.split(" ",data)
	if data[0] == "SYNACK": 
		ack_no = (int(data[1])+1)%8 
		seq_no = int(data[2])%8	
		msg = "SYNACKACK " + str(seq_no) + " " + str(ack_no+1) 
		c.sendto(msg,thread_addr)

	# -----------------DATA-----------------------

		method = "QUERY"
		print("Enter the filename to be queried ")
		filename = raw_input("Type LIST if you want the whole list : ")	
		http_msg = method + " " + hostname + " " + address + "\r\n" + filename + "\r\n" 
		c.sendto(http_msg, thread_addr)
		
		if filename == 'LIST': 

			http_packet, addr = c.recvfrom(MTU)
	#		print "Received packet ; " + http_packet	
			packet = re.split("\r\n\r\n", http_packet)
			packet_header = packet[0]
			packet_header = packet_header.replace("\r\n"," ")
			packet_header = re.split(" ", packet_header)
			if len(packet[1]) >0 :# and int(packet_header[4])==ack_no:
				DATA = packet[1]

				while packet_header[3]!="0":
					seq_no = int(packet_header[5])%8
					ack_no = (int(packet_header[4])+1)%8
					header = "ACK" + " " + str(seq_no) +" "+ str(ack_no)+"\r\n"
					c.sendto(header,addr)
				
					http_packet,addr = c.recvfrom(MTU)
					packet = re.split("\r\n\r\n" , http_packet)
					packet_header = packet[0]
					packet_header = packet_header.replace("\r\n"," ")
					packet_header = re.split(" ", packet_header)
					if len(packet[1])>0 and int(packet_header[4])==ack_no:
						DATA = DATA + packet[1]	
					seq_no = int(packet_header[5])%8
					ack_no = (int(packet_header[4]) + 1)%8 
			
				DATA = DATA.replace("\r\n", " ")
				DATA = re.split(" ", DATA)
				count = 0
				print "This is the list of all entries in the table"  			
				for i in xrange(len(DATA)/4):	
					print  DATA[count:count+4]
					count+=4
				c.close()		
		
		else:
						# Single file Requested 
			data = c.recv(MTU)
			data1 = data.replace("\r\n", " ")
			data = re.split(" ", data1)
		
			if data[0] =='200':
				print "File exists"
				data = data[2:]
				print "File size : " + data[2]
				count = 0
				for i in xrange(len(data)/4):
					print data[count:count+4]
				c.close()
			elif data[0] =='400':
				print "File does not exist"
				c.close()
	
	
	# -------------------GET FILE FROM PEER---------

	user_i = raw_input("Do you want to download any file? if YES enter the filename, if NO enter q: ")
	if user_i != "q":
		peer_addr = raw_input("Enter the ip address of peer: ")
		peer_port = int(raw_input("Enter the port no. of peer: "))
		c = socket.socket()
		c.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
	#	peer_addr = re.split(",", peer_addr)
	#	peer_addr = tuple(peer_addr)
		c.connect((peer_addr,peer_port))
		header = "GET" + " " + filename 
		c.send(header)
		data = c.recv(2048)
		data1 = data.replace("\r\n" , " ")
		packet_data = data1.split(" ")
		if packet_data[0] =="200":
			size = int(packet_data[3])
			f= open("copy_" + filename , 'wb')
			data = c.recv(2048)
			total = len(data)
			f.write(data)
			while total < size:
				data = c.recv(2048)
				total += len(data)
				f.write(data)
		print "File "+ "copy_"+filename+ " Copied to current directory"
		c.close()



# ---------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------MAIN FUCNTION------------------------------------------------------

if __name__ == '__main__':
	
		# -------------Spin off P2P server thread---------------
	server_t = threading.Thread(target = server_thread, args = ())
	server_t.daemon=True
	server_t.start()
	print"Enter 1 for Inform and update"
	print"Enter 2 for Query"
	print"Enter 3 for Exit"
	while(1):
		user_input = raw_input("Enter your response : ")
		if user_input =='1':
			inform_update()
		elif user_input =='2':
			content_query()
		elif user_input =='3':
			exit_msg()
		else:
			print "Ever tried. Ever failed. No matter. Try again. Fail again. Fail better." 
			
					
